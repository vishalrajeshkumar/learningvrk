// ROUTES
weatherApp.config(function ($routeProvider,$locationProvider,appIdTokenProvider) {
   
    $routeProvider
    
    .when('/', {
        templateUrl: 'pages/home.htm',
        controller: 'homeController'
    })
    
    .when('/forecast', {
        templateUrl: 'pages/forecast.htm',
        controller: 'forecastController'
    })
    // not implemented could be used for multiple choice of days 
    .when('/forecast/:days', {
        templateUrl: 'pages/forecast.htm',
        controller: 'forecastController'
    })
    //$locationProvider.html5Mode(true);
    appIdTokenProvider.setAppKeyID("cc0d60a1860ee33cb1a30441000493d4");
   
});

