//PROVIDER
weatherApp.provider("appIdToken", function () {
  var tokenID;
  return {
    setAppKeyID: function (value) {
      tokenID = value;
    },
    $get: function () {
      return {
        appKeyCode: tokenID
      };
    }
  };
});