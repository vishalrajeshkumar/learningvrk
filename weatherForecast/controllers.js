// CONTROLLERS
weatherApp.controller('homeController', ['$scope', 'cityService', '$location',function($scope, cityService, $location) {
    
    $scope.city = cityService.city;
    
    $scope.$watch('city', function() {
       cityService.city = $scope.city; 
    });
    
    
    $scope.submit = function(){
        $location.path('forecast');
        
    }
}]);


weatherApp.controller('forecastController', ['$scope', '$resource', '$routeParams', 'cityService','appIdToken', function($scope, $resource, $routeParams, cityService, appIdToken) {
    
    $scope.city = cityService.city;
    $scope.showFlag=true;
    //console.log($scope.city);
    
    //console.log($scope.days)
    
    console.log(appIdToken.appKeyCode);
    cityService.GetWeatherAPI($scope.city, appIdToken.appKeyCode).then(function(data){
      $scope.weatherResult = data;
         console.log($scope.weatherResult);
         $scope.showFlag = data.cod == 200 ? true : false;
         //console.log($scope.showFlag);
         //$scope.$digest();
         
   })
   .catch(function(response){
         $scope.weatherResult = response.data;
         console.log(response);
         $scope.showFlag= false;
   });
    
    $scope.convertToTime = function(time) { 
        //dt_txt
        return  time.slice(time.length-8, time.length)+" Hr";
        
    };
    
    $scope.countryCode = function (){
            
        return $scope.weatherResult.cod == 200 ? $scope.weatherResult.city.country : 'not found'  ;
    }
    
    $scope.getCurrentWeather = function(desc) { 

        $scope.forcastDescription;
        
        angular.forEach(desc, function(value, key){
            $scope.forcastDescription=value.description;
        });
        
        return $scope.forcastDescription;
    };
    
    $scope.convertToFahrenheit = function(degK) {
        
        return Math.round((1.8 * (degK - 273)) + 32);
        
    }
    
    $scope.convertToDate = function(dt) { 
      
        return new Date(dt * 1000);
        
    };
    
    
    
}]);