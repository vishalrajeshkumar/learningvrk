// SERVICES
weatherApp.service('cityService',[ '$http','$q', function( $http,$q) {
       
    this.city = "New York";
    
    this.GetWeatherAPI = function(city, appId)
    {
        //console.log(city);
        var deferred = $q.defer();
        $http({
  method: 'GET',
  url: 'http://api.openweathermap.org/data/2.5/forecast?q='+city+'&appid='+appId 
}).then(function successCallback(response) {
    // this callback will be called asynchronously
    // when the response is available
           console.log(response.data);
           deferred.resolve(response.data);
            
  }, function errorCallback(response) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
            deferred.reject(response);
  });
       return deferred.promise;
        };
    
    
}]);